import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
// import 'dart:html';

import 'package:fixnum/fixnum.dart';
import 'package:uuid/uuid.dart';

import 'generated/message.pb.dart';

import 'common.dart';

main() async {
  final Map<String, WebSocket> connections = {};
  final List<ChatMessage> messages = <ChatMessage>[];

  runZonedGuarded(() async {
    var server = await HttpServer.bind(host, port);
    server.listen((HttpRequest req) async {
      if (req.uri.path == '/ws') {
        var socket = await WebSocketTransformer.upgrade(req);
        await socket.addStream(
            Stream.fromIterable(messages.map((e) => e.writeToBuffer())));
        socket.listen((data) {
          if (data is Uint8List) {
            final msg = ChatMessage.fromBuffer(data);
            connections[msg.user.id] = socket;
            msg.id = Uuid().v4();
            msg.epochMilliseconds =
                Int64(DateTime.now().millisecondsSinceEpoch);
            print("${DateTime.now()} " + msg.toDebugString());
            messages.add(msg);
            connections.forEach((id, ws) async {
              await ws.addStream(
                  Stream.value(msg.writeToBuffer()));
            });
          }
        });
      }
    });
  }, (e, s) => print("Error: $e"));
}
